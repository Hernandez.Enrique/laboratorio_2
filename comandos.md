## Iniciar git
```
git init
```
## Crear ramas
```
git branch --create <nombre de la rama>
```
## Agregar todos los archivos
```
git add .
```
## Para agregar un archivo en especifico
```
git add <nombre del archivo> |o| git add <nombre de la carpeta> 
```
## Hacer commit
```
git commit -m "mensaje"
```
#### Al para hacer otro commit se sigue este proceso en orden
```
git add .
git commit -m "mensaje"
```
## Para que es el pull y el push
```
git pull origin master # Para traer los cambios del repositorio remoto
git push origin master # Para subir los cambios al repositorio remoto
git push --set-upstream origin main # Para subir los cambios al repositorio remoto
```

### Dotnet console template
```
dotnet new console -o <nombre del proyecto>
```

# Configuración de git
```bash
git config --global user.name "Hernandez.Enrique"
git config --global user.email "enrique.hernandez@jala.university"
```

* Recordar añadir la clave ssh en github o gitlab